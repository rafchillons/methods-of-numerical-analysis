import numpy as np
import math
from colorama import Fore

def symmetrize(A):
    return np.dot(A.T, A)

def Rotation(m, eps):
    n = m.shape[0]
    vector = np.eye(m.shape[0])
    A = m.copy()
    iterations = 0
    while True:
        n = A.shape[0]
        maxelem = 0
        max_i = 0
        max_j = 0
        for i in range(n):
            for j in range(i + 1, n):
                if abs(A[i][j]) > maxelem:
                    maxelem = abs(A[i][j])
                    max_i = i
                    max_j = j
        if maxelem <= eps:
            break
        print("aMAX: {0}   i={1} j={2}".format(round(maxelem, 3), max_i, max_j))
        fi = 0.5 * math.atan(2.0 * maxelem / (A[max_i][max_i] - A[max_j][max_j]))
        print("fi: {0}".format(round(fi, 3)))
        print("COS(fi): {}".format(round(math.cos(fi), 3)))
        print("SIN(fi): {}".format(round(math.sin(fi), 3)))
        U = np.eye(n)
        U[max_i][max_i] = math.cos(fi)
        U[max_j][max_j] = math.cos(fi)
        U[max_i][max_j] = -math.sin(fi)
        U[max_j][max_i] = math.sin(fi)
        vector = np.dot(vector, U)
        iterations += 1
        A = np.dot(np.dot(U.T, A), U)
        print("U\n {0}".format(vector.round(3)))
        print("A\n {0}\n===============( {1} )===============\n\n".format(A.round(3), iterations))
    print("Iterations N: {0}\n".format(iterations))
    return A, vector


def main():
    A = np.array([[4.238, 0.329, 0.256, 0.425],
                  [0.238, 0.329, 0.256, 0.425],
                  [0.365, 0.217, 2.897, 0.168],
                  [0.178, 0.294, 0.432, 3.701]])
    A = symmetrize(A)
    print("AT: \n{0}\n".format(A))
    A, vector = Rotation(A, 1)

    print("\nOwn values:\n{0}".format([A[i][i] for i in range(len(A))]))
    print("\nOwn vectors:\n{0}".format(vector.round(3)))


if __name__ == "__main__":
    main()
