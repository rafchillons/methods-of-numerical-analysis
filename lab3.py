import numpy as np
from colorama import Fore

def symmetrize(A, b):
    return np.dot(A.T, A), np.dot(A.T, b)


def build_U(A):
    rows, cols = A.shape
    U = np.zeros((rows, cols))
    determinate = 1;
    for i in range(rows):
        U[i][i] = np.sqrt(A[i][i] - np.sum([U[k][i] ** 2 for k in range(i)]))
        determinate = determinate * np.square(U[i][i])
        print("U[{0}][{0}]: {1}".format(i + 1, round(U[i][i], 3)))
        for j in range(i + 1, cols):
            U[i][j] = (A[i][j] - np.sum([U[k][i] * U[k][j] for k in range(i)])) / U[i][i]
            print("U[{0}][{1}]: {2}".format(i + 1, j + 1, round(U[i][j], 3)))
    print("\ndet A: {0}\n".format(round(determinate, 3)))
    return U


def solve_sqrt(U, b):
    rows, cols = U.shape
    y = np.zeros(rows)
    UT = U.T
    for i in range(rows):
        y[i] = b[i] / UT[i][i]
        for k in range(i + 1, rows):
            b[k] -= UT[k][i] * y[i]
    print('Y\n{0}\n\n'.format(y.round(3)))
    x = np.zeros(rows)  
    for i in range(rows - 1, -1, -1):
        x[i] = y[i] / U[i][i]
        for k in range(i):
            y[k] -= U[k][i] * x[i]
    return x


def reverse(u, uT):
    rows, cols = u.shape
    e = np.zeros(rows)
    y = np.zeros(rows)

    print("\n\nU.Ty = e.T")
    for i in range(rows):
        e = np.zeros(rows)
        e[i] = 1
        y = np.linalg.solve(uT, e)
        x = np.linalg.solve(u, y)
        print("y{1}: {0}".format(y.round(3), i + 1))
        print("x{1}: {0}\n".format(x.round(3), i + 1))
    return

def symmetrize_checker(A):
    print("\n")
    rows, cols = A.shape
    for k in range(0, 3):
        for q in range(k + 1, 4):
            print("{0} : {1}".format(A[q][k], A[k][q]))
            if A[q][k] != A[k][q]:
                return print("Matrix unsymmetrized.")
    return print("Matrix symmetrized")


def main():
    A = np.array([[4.238, 0.329, 0.256, 0.425],
                  [0.249, 2.964, 0.351, 0.127],
                  [0.365, 0.217, 2.897, 0.168],
                  [0.178, 0.294, 0.432, 3.701]])
    b = np.array([0.560, 0.380, 0.778, 0.749])

    print("A.T\n{0}".format(A.T))
    A_symm, b_symm = symmetrize(A, b)
    A_symm = A_symm.round(3)

    symmetrize_checker(A)
    symmetrize_checker(A_symm)

    print("\nA(sim)\n{0}\n\nb(sim)\n{1}\n".format(A_symm.round(3), b_symm.round(3)))

    U = build_U(A_symm)
    print("U\n{0}\n\nU.T\n{1}\n\n".format(U.round(3), U.T.round(3)))

    x1 = solve_sqrt(U, b_symm)
    print('X\n{0}'.format(x1.round(3)))

    reverse(U, U.T)


if __name__ == '__main__':
    main()
