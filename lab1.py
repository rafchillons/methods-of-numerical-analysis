import numpy as np


def matrix_norm(A):
    A = np.abs(A)
    return np.max([sum(A[i, :]) for i in range(A.shape[0])])


def vector_norm(v):
    return np.max(np.abs(v))

def matrix_inverse(A):
    return np.linalg.inv(A)

def main():
    A = np.array([[4.238, 0.329, 0.256, 0.425],
                  [0.238, 0.329, 0.256, 0.425],
                  [0.365, 0.217, 2.897, 0.168],
                  [0.178, 0.294, 0.432, 3.701]])
    B = np.array([0.560, 0.380, 0.778, 0.749])
    dx = 0.001
    db = 0.001

    check = np.linalg.det(A)

    if check == 0:
        print('No solution')
        return

    x = np.linalg.solve(A, B)
    check = np.array.det(A)
    print(check)
    print('x1...xN: \n{}\n', x)


    db_rel = db / vector_norm(B)
    error = matrix_norm(matrix_inverse(A)) * db
    error_rel = matrix_norm(A) * matrix_norm(matrix_inverse(A)) * db_rel
    print('Error: {}'.format(error))
    print('Error relative: {}').format(error_rel)
  
if __name__ == '__main__':
    main()