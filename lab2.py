import numpy as np
from math import sqrt

def transformate_system(A, B):
    n = len(A)
    a = A.copy()
    C = B.copy()
    for i in range(n):
        C[i] /= a[i][i]
        a[i, :] = -a[i, :] / a[i][i]
        a[i][i] = 0
    a = np.round(a, 4)
    C = np.round(C, 4)
    print('Transformed matrix A: \n{}\n'.format(a))
    print('Transformed matrix C: \n{}\n'.format(C))
    return a, C


def vector_norm(v):
    return np.max(np.abs(v))


def is_convergence(v, e):
    v = vector_norm(v)
    if v >= e:
        return False
    return True


def check(x, x_new, e):
    n = len(x)
    result = np.zeros_like(x)
    for i in range(n):
        result[i] = x_new[i] - x[i]
    print result
    temp = vector_norm(result)
    if is_convergence(temp, e) == True:
        return True
    return False


def vectorize(a):
    n = len(a)
    v = []
    temp = 0
    for j in range(n):
        for i in range(n):
            temp += (-a[j][i])
            temp = np.round(temp, 3)
        v.append(temp)
        temp = 0
    v = np.array(v)
    return v


def solve_simple_iterations(a, C, C_norm, B_norm, e, x):
    steps_number = int(np.round(np.log(e * (1 - B_norm) / (C_norm)) / (np.log(B_norm)) + 1))
    print ('k0 = {}\n'.format(steps_number))

    for i in range(steps_number):
        x = np.round(np.dot(a, x) + C, 4)
        print(x)

    print('\nX{}:'.format(steps_number))
    print(x)
    return x

def solve_seidel(a, b, x, e, max_iterations):
    n = len(a)
    converge = False

    for iteration in range(max_iterations):
        print iteration
        x_new = np.zeros_like(x)
        for i in range(n):
            s1 = np.dot(a[i, :i], x_new[:i])
            s2 = np.dot(a[i, i + 1:], x[i + 1:])
            x_new[i] = (b[i] - s1 - s2) / a[i, i]
            if not check(x, x_new, e):
                return x
            x = x_new
        print x

def main():
    A = np.array([[4.238, 0.329, 0.256, 0.425],
                  [0.249, 2.964, 0.351, 0.127],
                  [0.365, 0.217, 2.897, 0.168],
                  [0.178, 0.294, 0.432, 3.701]])
    B = np.array([0.560, 0.380, 0.778, 0.749])
    e = 0.01

    print('Original matrix A: \n{}\n'.format(A))

    a, C = transformate_system(A, B)
    x = np.zeros_like(C)

    B_norm = vectorize(a)
    if not is_convergence(B_norm, 1):
        print('||B|| > 1')
        exit(0)

    B_norm = vector_norm(B_norm)
    C_norm = vector_norm(C)

    solve_simple_iterations(a, C, C_norm, B_norm, e, x)
    print('\n')
    solve_seidel(A, B, x, e, 10)


if __name__ == '__main__':
    main()
