import math as math
from sympy import *

def f(x):
    return cos(x) - ((x + 2.0) ** 0.5) + 1.0


def df(x):
    return -(0.5 / ((x + 2.0) ** 0.5)) - sin(x)


def ddf(x):
    return 0.25 / ((x + 2.0) ** 1.5) - cos(x)


def mtd_graphical():
    x, y = symbols('x y')
    p = plot((cos(x) - (x + 2.0) ** 0.5 + 1.0), line_color='green')

    a = 0
    b = 1.0
    if (f(a) * f(b)) < 0:
        print("f(a) * f(b)) < 0\na, b = [{}, {}]\n".format(a, b))
    return p.show()


def mtd_hord():
    e = 0.001
    a = 0.0
    b = 1.0
    x = [a, b]
    x_new = 0
    i = 1

    while abs(x[i] - x[i-1]) >= e:
        x_new = x[i] - (f(x[i]) * (x[i] - x[0])) / (f(x[i]) - f(x[0]))
        x.append(x_new)
        print(x[i]) #debug
        i = i + 1

    return print("\nEee boi. \nS9000 found root: {} in {} iterations".format(x[i], i))


def mtd_tangential(): #tangential - касательная
    e = 0.001
    a = 0.0
    b = 1.0
    x = []
    n = 0
    x_new = 0

    if (f(a) * ddf(a)) > 0:
        x.append(a)
    else:
        x.append(b)
    print("Initial approximation (Xo): {}".format(x[0]))  # начальное приближение Xo

    while abs(f(x[n]) / df(x[n])) >= e:
        print(f(x[n]) / df(x[n]))
        x_new = x[n] - (f(x[n]) / df(x[n]))
        x.append(x_new)
        n = n + 1

    return print("\nS9000 found root: {} in {} iterations".format(x[n], n))


def mtd_graphical_two():
    x, y = symbols('x y')
    p = plot_implicit(Eq(tan(x * y) - x ** 2, 0), line_color='green', show=False)
    p2 = plot_implicit(Eq(0.7 * (x ** 2) + 2 * (y ** 2), 1), line_color='blue', show=False) #EQ - expression value
    p.extend(p2)
    return p.show()

def sys_f(x):
    return tan(x * y) - x ** 2

def sys_g(x):
    return 0.7 * (x ** 2) + 2 * (y ** 2)

def mtd_tangential_system():
    e = 0.001
    k = 0

    x, y = symbols('x y')
    f = tan(x * y) - x ** 2
    g = 0.7 * (x ** 2) + 2 * (y ** 2) - 1

    xy = Matrix([1.0, 0.5])
    J = Matrix([[diff(f, x), diff(f, y)], [diff(g, x), diff(g, y)]])
    F = Matrix([f ,g])
    J = J.inv()

    xy_new = (xy - J * F).subs(x, xy[0]).subs(y, xy[1])

    while abs(xy_new[0] - xy[0]) >= e:
        print("\n")
        xy = xy_new
        pprint(xy)
        jN = J.subs(x, xy[0]).subs(y, xy[1])
        fN = F.subs(x, xy[0]).subs(y, xy[1])
        xy_new = (xy - jN * fN)
        pprint(xy_new)
        k = k + 1

    return print("\nS9000 found X: {} and Y: {} \nin {} iterations".format(xy_new[0], xy_new[1], k))


def mtd_tangential_system_upgrade():
    e = 0.001
    k = 0

    x, y = symbols('x y')
    f = tan(x * y) - x ** 2
    g = 0.7 * (x ** 2) + 2 * (y ** 2) - 1

    xy = Matrix([1.0, 0.5])
    J = Matrix([[diff(f, x), diff(f, y)], [diff(g, x), diff(g, y)]])
    F = Matrix([f ,g])
    J = J.inv().subs(x, xy[0]).subs(y, xy[1])

    xy_new = (xy - J * F).subs(x, xy[0]).subs(y, xy[1])

    while abs(xy_new[0] - xy[0]) >= e:
        xy = xy_new
        fN = F.subs(x, xy[0]).subs(y, xy[1])
        xy_new = (xy - J * fN)
        k = k + 1

    return print("\nS9000 found X: {} and Y: {} \nin {} iterations".format(xy_new[0], xy_new[1], k))



def mtd_simple_iterations():
    e = 0.001
    k = 0

    x, y = symbols('x y')
    f = sqrt(tan(x * y)) #f = x
    g = sqrt(1 - (0.7 * (x ** 2)) / 2) #g = y
    x0 = 1.0
    y0 = 0.5

    x1 = f.subs(x, x0).subs(y, y0)
    y1 = g.subs(x, x0).subs(y, y0)

    while abs(x0 - x1) >= e:
        x0 = x1
        y0 = y1

        x1 = f.subs(x, x0).subs(y, y0)
        y1 = g.subs(x, x0).subs(y, y0)

        k = k + 1

    return print("\nS9000 found X: {} and Y: {} \nin {} iterations".format(x1, y1, k))


def main():
    #mtd_graphical()
    #mtd_hord()
    #mtd_tangential()
    #mtd_graphical_two()
    #mtd_tangential_system()
    #mtd_tangential_system_upgrade()
    mtd_simple_iterations()

if __name__ == '__main__':
    main()


