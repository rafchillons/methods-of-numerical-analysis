import numpy as np
import matplotlib.pyplot as plt


def interpol_lagrandj(xv, yv, x):
    n = len(xv)
    L = 0
    for i in range(n):
        P = 1 #multiplication
        for j in range(n):
            if i != j:
                P *= (x - xv[j]) / (xv[i] -  xv[j])
        #         print("P = {0} - {1} / {2} - {1}".format(x, xv[j], xv[i]))
        # print("P[{}]: {}".format(i, P))
        L += yv[i] * P
    return L


# def interpol_lagrandj(xv, yv):
#     n = len(xv)
#     L = 0
#     j = 0
#     i = 0
#     x = symbols('x')
#     P = (x - xv[j]) / (xv[i] -  xv[j])
#     for i in range(n):
#         for j in range(n):
#             if i != j:
#                 P *= P
#                 if j == 3:
#                     print("P = {0} - {1} / {2} - {1}".format(x, xv[j], xv[i]))
#         print("* {0}\n".format(yv[i]))
#     return


def interpol_lagrandj_plot(x, y):
    xnew = np.linspace(np.min(x), np.max(x), 100)
    ynew = [interpol_lagrandj(x, y, i) for i in xnew]
    plt.plot(x, y, 'o', xnew, ynew)
    plt.grid(True)
    return plt.show()


def finite_difference(y):
    n = len(y)
    for k in range(1, n, 1):
        for i in range(n - k):
            y[i] = y[i + 1] - y[i]
            print("Δ{0} y[{1}] = {2}".format(k, i, y[i]))
    return


def divided_difference(x, y):
    print('\n')
    n = len(x)
    A = np.zeros((n, n))
    A[:, 0] = y
    print("{}\n".format(A))
    for i in range(1, 5, 1):
        for k in range(len(x) - i):
            A[k, i] = (A[k + 1, i - 1] - A[k, i - 1]) / (x[k + i] - x[k])
            print("Δ{0} y[{1}] = {2}".format(k, i, A[k][i]))
    print("\n{}".format(A))
    return (A)


def polynomial_newton(xv, yv, x):
    a = divided_difference(xv, yv)
    answer = 0
    n = len(xv)
    for i in range(n):
        b = 1
        for j in range(i):
            b *= x - xv[j]
        answer += a[0][i] * b
    print("Newton's polynom: {}".format(answer))
    return answer


def piecewise_linear_interpolation(x, y):
    n = len(x)
    a0 = np.zeros(n - 1)
    a1 = np.zeros(n - 1)
    for i in range(1, n):
        a1[i-1] = (y[i] - y[i-1]) / (x[i] - x[i-1])
        a0[i-1] = y[i-1] - a1[i - 1] * x[i-1]
    print('a[1] =', a1)
    print('a[0] =', a0)
    return a1, a0


def get_line_y(x, k, b):
    return k * x + b


def get_parabola_y(x, a, b, c):
    return a * x ** 2 + b * x + c


def get_cube_y(x0, a, b, c, d, xk):
    return a + b * (x0 - xk) + c * (x0 - xk) ** 2 + d * (x0 - xk) ** 3


def piecewise_linear_interpolation_plot(x , y):
    a, b = piecewise_linear_interpolation(x, y)
    ypts_linear = []
    xpts_linear = []
    for i in range(1, len(x)):
        cur_xpts = np.arange(x[i - 1], x[i], 0.01)
        xpts_linear += list(cur_xpts)
        ypts_linear += [get_line_y(t, a[i - 1], b[i - 1]) for t in cur_xpts]
        plt.plot(xpts_linear, ypts_linear, label='Piecewise linear interpolation spline')
    plt.grid(True)
    return plt.show()


def polynomial_newton_plot(x, y):
    xpts = np.arange(np.min(x), np.max(x), 0.01)
    ypts_newton = np.array([polynomial_newton(x, y, t) for t in xpts])
    plt.plot(xpts, ypts_newton, label='Newton')
    return plt.show()

def piecewise_quadratic_interpolation(x, y):
    n = len(x)
    a = np.zeros((n - 2, 3))
    for i in range(1, n - 1):
        a[i - 1, 2] = (y[i + 1] - y[i - 1]) / ((x[i + 1] - x[i - 1]) * (x[i + 1] - x[i])) - \
                      (y[i] - y[i - 1]) / ((x[i] - x[i - 1]) * (x[i + 1] - x[i]))
        a[i - 1, 1] = (y[i] - y[i - 1]) / (x[i] - x[i - 1]) - a[i - 1, 2] * (x[i] + x[i - 1])
        a[i - 1, 0] = y[i - 1] - a[i - 1, 1] * x[i - 1] - a[i - 1, 2] * x[i - 1] ** 2
    print("a[0] = {}".format(a[0]))
    print("a[1] = {}".format(a[1]))
    print("a[2] = {}".format(a[2]))
    return a


def piecewise_quadratic_interpolation_plot(x, y):
    a = piecewise_quadratic_interpolation(x, y)
    ypts_quadratic = []
    xpts_quadratic = []
    for i in range(1, len(x) - 1, 2):
        cur_xpts = np.arange(x[i - 1], x[i + 1], 0.01)
        xpts_quadratic += list(cur_xpts)
        ypts_quadratic += [get_parabola_y(t, a[i - 1, 2], a[i - 1, 1], a[i - 1, 0]) for t in cur_xpts]
    if i < len(x) - 3:
        cur_xpts = np.arange(x[-2], x[-1], 0.01)
        xpts_quadratic += list(cur_xpts)
        ypts_quadratic += [get_parabola_y(t, a[-1, 2], a[-1, 1], a[-1, 0]) for t in cur_xpts]
    plt.plot(xpts_quadratic, ypts_quadratic, label='Piecewise quadratic interpolation spline')
    plt.grid(True)
    return plt.show()


def cubic_spline_interpolation(x, y):
    n = len(x)
    h = np.zeros(n)
    l = np.zeros(n)
    delta = np.zeros(n)
    lam = np.zeros(n)
    a = np.zeros(n)
    b = np.zeros(n)
    c = np.zeros(n)
    d = np.zeros(n)
    a[:-1] = y[1:]
    for i in range(n - 1):
        h[i] = x[i + 1] - x[i]
        l[i] = (y[i + 1] - y[i]) / h[i]

    delta[0] = - 1 / 2 * h[1] / (h[0] + h[1])
    lam[0] = 3 / 2 * (l[1] - l[0]) / (h[1] + h[0])

    for i in range(2, n):
        delta[i - 1] = - h[i] / (2 * h[i - 1] + 2 * h[i] + h[i - 1] * delta[i - 2])

    for i in range(2, n):
        lam[i - 1] = (2 * l[i] - 3 * l[i - 1] - h[i - 1] * lam[i - 2]) / (
        2 * h[i - 1] + 2 * h[i] + h[i - 1] * delta[i - 2])

    for i in range(n - 1, 0, -1):
        c[i - 1] = delta[i - 1] * c[i] + lam[i - 1]

    for i in range(1, n - 1):
        b[i] = l[i] + 2 / 3 * c[i] * h[i] + 1 / 3 * h[i] * c[i - 1]
        d[i] = (c[i] - c[i - 1]) / (3 * h[i])

    b[0] = l[0] + 2 / 3 * c[0] * h[0]
    d[0] = c[0] / (3 * h[0])
    print("a = {}\nb = {}\nc = {}\nd = {}\n".format(a , b.round(5), c, d))
    return a, b, c, d


def cubic_spline_interpolation_plot(x, y):
    a, b, c, d = cubic_spline_interpolation(x, y)
    xpts_cubic = []
    ypts_cubic = []
    for i in range(len(x) - 1):
        cur_xpts = np.arange(x[i], x[i + 1], 0.01)
        xpts_cubic += list(cur_xpts)
        ypts_cubic += [get_cube_y(t, a[i], b[i], c[i], d[i], x[i + 1]) for t in cur_xpts]

    plt.plot(xpts_cubic, ypts_cubic, label='Cubic spline interpolation')
    plt.grid(True)
    return plt.show()


def final_plot(x, y):
    xnew = np.linspace(np.min(x), np.max(x), 100)
    ynew = [interpol_lagrandj(x, y, i) for i in xnew]
    plt.plot(x, y, 'o', xnew, ynew)
    plt.grid(True)

    plt.plot(x, y, 'ro', label='points')
    xpts = np.arange(np.min(x), np.max(x), 0.01)

    ypts_newton = np.array([polynomial_newton(x, y, t) for t in xpts])
    plt.plot(xpts, ypts_newton, label='Newton')

    a, b = piecewise_linear_interpolation(x, y)
    ypts_linear = []
    xpts_linear = []
    for i in range(1, len(x)):
        cur_xpts = np.arange(x[i - 1], x[i], 0.01)
        xpts_linear += list(cur_xpts)
        ypts_linear += [get_line_y(t, a[i - 1], b[i - 1]) for t in cur_xpts]
        plt.plot(xpts_linear, ypts_linear, label='Piecewise linear interpolation spline')

    a = piecewise_quadratic_interpolation(x, y)
    ypts_quadratic = []
    xpts_quadratic = []
    for i in range(1, len(x) - 1, 2):
        cur_xpts = np.arange(x[i - 1], x[i + 1], 0.01)
        xpts_quadratic += list(cur_xpts)
        ypts_quadratic += [get_parabola_y(t, a[i - 1, 2], a[i - 1, 1], a[i - 1, 0]) for t in cur_xpts]
    if i < len(x) - 3:
        cur_xpts = np.arange(x[-2], x[-1], 0.01)
        xpts_quadratic += list(cur_xpts)
        ypts_quadratic += [get_parabola_y(t, a[-1, 2], a[-1, 1], a[-1, 0]) for t in cur_xpts]
    plt.plot(xpts_quadratic, ypts_quadratic, label='Piecewise quadratic interpolation spline')

    a, b, c, d = cubic_spline_interpolation(x, y)
    xpts_cubic = []
    ypts_cubic = []
    for i in range(len(x) - 1):
        cur_xpts = np.arange(x[i], x[i + 1], 0.01)
        xpts_cubic += list(cur_xpts)
        ypts_cubic += [get_cube_y(t, a[i], b[i], c[i], d[i], x[i + 1]) for t in cur_xpts]

    plt.plot(xpts_cubic, ypts_cubic, label='Cubic spline interpolation')
    plt.grid(True)
    return plt.show()



def main():
    x = [0.015, 0.681, 1.342, 2.118, 2.671]
    y = [-2.417, -3.819, -0.642, 0.848, 2.815]

    #print("L = {0}".format(interpol_lagrandj(x, y, x[0] + x[1])))
    #interpol_lagrandj_plot(x, y)

    #finite_difference(y)
    #divided_difference(x, y)
    #polynomial_newton(x, y, x[0] + x[1])
    polynomial_newton_plot(x, y)
    #piecewise_linear_interpolation(x, y)
    #piecewise_linear_interpolation_plot(x, y)
    #piecewise_quadratic_interpolation(x, y)
    #piecewise_quadratic_interpolation_plot(x, y)
    #cubic_spline_interpolation(x, y)
    #cubic_spline_interpolation_plot(x, y)
    #final_plot(x, y)


if __name__ == '__main__':
    main()